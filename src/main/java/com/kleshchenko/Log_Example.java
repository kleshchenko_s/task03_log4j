package com.kleshchenko;

import org.apache.logging.log4j.*;

/**
 *  Log4j example
 *  Task04_Log4j
 * @author Serhii Kleshchenko
 */
public class Log_Example {
    private static final Logger log = LogManager.getLogger(Log_Example.class);
    public static void main(String[] args) {
        log.trace("This is a trace message");
        log.debug("This is a debug message");
        log.info("This is an info message");
        log.warn("This is a warn message");
        log.error("This is an error message");
        log.fatal("This is a fatal message");
    }
}